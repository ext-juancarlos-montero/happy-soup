@isTest
public with sharing class CouponActivationQuickAction_Test {
  @isTest
  private static void itShouldThrownAlreadyActiveException() {
    Coupon__c activeCoupon = new Coupon__c(Active__c = true, Country__c = 'ES');
    insert activeCoupon;
    try {
      new CouponActivationQuickAction().activate(activeCoupon.Id);
    } catch (Exception e) {
      System.assertEquals('Coupon already active', e.getMessage());
    }
  }

  @isTest
  private static void itShouldThrownDiscountRequiredException() {
    Coupon__c couponWithoutDiscount = new Coupon__c(
      Active__c = false,
      Country__c = 'ES'
    );
    insert couponWithoutDiscount;
    try {
      new CouponActivationQuickAction().activate(couponWithoutDiscount.Id);
    } catch (Exception e) {
      System.assertEquals(
        'Discount is required for activation',
        e.getMessage()
      );
    }
  }

  @isTest
  private static void itShouldActivateTheCoupon() {
    Coupon__c couponToActivate = new Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Discount__c = 1.0
    );
    insert couponToActivate;
    new CouponActivationQuickAction().activate(couponToActivate.Id);
    System.assert(
      [SELECT Active__c FROM Coupon__c WHERE Id = :couponToActivate.Id]
      .Active__c
    );
  }

  @isTest
  private static void itShouldThrownAlreadyDeactiveException() {
    Coupon__c inactiveCoupon = new Coupon__c(
      Active__c = false,
      Country__c = 'ES'
    );
    insert inactiveCoupon;
    try {
      new CouponActivationQuickAction().deactivate(inactiveCoupon.Id);
    } catch (Exception e) {
      System.assertEquals('Coupon already inactive', e.getMessage());
    }
  }

  @isTest
  private static void itShouldDeactivateTheCoupon() {
    Coupon__c couponToDeactivate = new Coupon__c(
      Active__c = true,
      Country__c = 'ES'
    );
    insert couponToDeactivate;
    new CouponActivationQuickAction().deactivate(couponToDeactivate.Id);
    System.assert(
      ![SELECT Active__c FROM Coupon__c WHERE Id = :couponToDeactivate.Id]
      .Active__c
    );
  }
}
