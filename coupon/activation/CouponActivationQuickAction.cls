public with sharing class CouponActivationQuickAction implements IActivactionDeactivation {
  public void activate(Id recordId) {
    Coupon__c couponToActivate = [
      SELECT Id, Active__c, Discount__c
      FROM Coupon__c
      WHERE Id = :recordId
    ];
    if (couponToActivate.Active__c == true) {
      throw new CouponException('Coupon already active');
    }
    if (couponToActivate.Discount__c == null) {
      throw new CouponException('Discount is required for activation');
    }
    couponToActivate.Active__c = true;
    update couponToActivate;
  }

  public void deactivate(Id recordId) {
    Coupon__c couponToDeactivate = [
      SELECT Id, Active__c
      FROM Coupon__c
      WHERE Id = :recordId
    ];
    if (couponToDeactivate.Active__c == false) {
      throw new CouponException('Coupon already inactive');
    }
    couponToDeactivate.Active__c = false;
    update couponToDeactivate;
  }

  public class CouponException extends Exception {
  }
}
