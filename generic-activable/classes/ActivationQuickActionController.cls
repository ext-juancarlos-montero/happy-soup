public with sharing class ActivationQuickActionController {
  private static final Map<String, String> iActivationDeactivationImplementations = new Map<String, String>{
    'Coupon__c' => 'CouponActivationQuickAction',
    'Plus_Coupon__c' => 'PlusCouponActivationQuickAction'
  };

  @AuraEnabled
  public static void activate(Id recordId) {
    try {
      String sObjectName = recordId.getSobjectType().getDescribe().getName();
      String className = iActivationDeactivationImplementations.get(
        sObjectName
      );
      if (className == null) {
        throw new ActivationException('Object activation not implemented');
      }

      Type activationQuickActionType = Type.forName(className);
      IActivactionDeactivation activator = (IActivactionDeactivation) activationQuickActionType.newInstance();
      activator.activate(recordId);
    } catch (Exception e) {
      AuraHandledException auraHandledException = new AuraHandledException(
        e.getMessage()
      );
      auraHandledException.setMessage(e.getMessage());
      throw auraHandledException;
    }
  }

  @AuraEnabled
  public static void deactivate(Id recordId) {
    try {
      String sObjectName = recordId.getSobjectType().getDescribe().getName();
      String className = iActivationDeactivationImplementations.get(
        sObjectName
      );
      if (className == null) {
        throw new ActivationException('Object deactivation not implemented');
      }

      Type activationQuickActionType = Type.forName(className);
      IActivactionDeactivation activator = (IActivactionDeactivation) activationQuickActionType.newInstance();
      activator.deactivate(recordId);
    } catch (Exception e) {
      AuraHandledException auraHandledException = new AuraHandledException(
        e.getMessage()
      );
      auraHandledException.setMessage(e.getMessage());
      throw auraHandledException;
    }
  }

  public class ActivationException extends Exception {
  }
}
