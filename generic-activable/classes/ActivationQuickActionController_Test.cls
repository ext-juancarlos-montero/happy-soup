@isTest
public with sharing class ActivationQuickActionController_Test {
  @isTest
  private static void itShouldThrownNotImplementedExceptionForActivation() {
    Account account = new Account(Name = 'Test');
    insert account;
    try {
      ActivationQuickActionController.activate(account.Id);
    } catch (Exception e) {
      System.assertEquals('Object activation not implemented', e.getMessage());
    }
  }

  @isTest
  private static void itShouldThrownNotImplementedExceptionForDeactivation() {
    Account account = new Account(Name = 'Test');
    insert account;
    try {
      ActivationQuickActionController.deactivate(account.Id);
    } catch (Exception e) {
      System.assertEquals(
        'Object deactivation not implemented',
        e.getMessage()
      );
    }
  }
}
