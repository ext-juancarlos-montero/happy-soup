@namespaceAccessible
public interface IActivactionDeactivation {
  void activate(Id recordId);
  void deactivate(Id recordId);
}
