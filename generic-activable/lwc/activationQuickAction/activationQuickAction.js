import { LightningElement, api } from "lwc";
import { getRecordNotifyChange } from "lightning/uiRecordApi";
import activate from "@salesforce/apex/ActivationQuickActionController.activate";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class ActivationQuickAction extends LightningElement {
  @api recordId;
  @api async invoke() {
    try {
      await activate({ recordId: this.recordId });
      getRecordNotifyChange([{ recordId: this.recordId }]);
      this.showSuccessNotification("Success");
    } catch (err) {
      console.log(err);
      this.showErrorNotification("Error", err.body.message);
    }
  }

  showSuccessNotification(title, message) {
    this.dispatchEvent(
      new ShowToastEvent({
        title: title,
        message: message,
        variant: "success"
      })
    );
  }

  showErrorNotification(title, message) {
    this.dispatchEvent(
      new ShowToastEvent({
        title: title,
        message: message,
        variant: "error"
      })
    );
  }
}
