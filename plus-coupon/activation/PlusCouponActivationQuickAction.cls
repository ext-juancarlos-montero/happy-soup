public with sharing class PlusCouponActivationQuickAction implements IActivactionDeactivation {
  public void activate(Id recordId) {
    Plus_Coupon__c plusCouponToActivate = [
      SELECT
        Id,
        Active__c,
        Required_Spending__c,
        (
          SELECT Id, Coupon__r.Name, Coupon__r.Active__c
          FROM Plus_Coupon_Prizes__r
        )
      FROM Plus_Coupon__c
      WHERE Id = :recordId
    ];
    this.validatePlusCoupon(plusCouponToActivate);
    this.validatePrizes(plusCouponToActivate.Plus_Coupon_Prizes__r);
    plusCouponToActivate.Active__c = true;
    update plusCouponToActivate;
  }

  public void deactivate(Id recordId) {
    Plus_Coupon__c plusCouponToDeactivate = [
      SELECT Id, Active__c
      FROM Plus_Coupon__c
      WHERE Id = :recordId
    ];
    if (plusCouponToDeactivate.Active__c == false) {
      throw new PlusCouponException('Plus Coupon already inactive');
    }
    plusCouponToDeactivate.Active__c = false;
    update plusCouponToDeactivate;
  }

  private void validatePlusCoupon(Plus_Coupon__c plusCoupon) {
    if (plusCoupon.Active__c == true) {
      throw new PlusCouponException('Plus Coupon already active');
    }
    if (plusCoupon.Required_Spending__c == null) {
      throw new PlusCouponException(
        'Required spending is required for activation'
      );
    }
  }

  private void validatePrizes(List<Plus_Coupon_Prize__c> prizes) {
    if (prizes.isEmpty()) {
      throw new PlusCouponException('At least one prize is required');
    }
    List<String> invalidPrizeCoupons = new List<String>();
    for (Plus_Coupon_Prize__c prize : prizes) {
      if (prize.Coupon__r.Active__c == false) {
        invalidPrizeCoupons.add(prize.Coupon__r.Name);
      }
    }
    if (!invalidPrizeCoupons.isEmpty()) {
      throw new PlusCouponException(
        'Some prizes aren\'t properly configured:' +
        String.join(invalidPrizeCoupons, ',')
      );
    }
  }

  public class PlusCouponException extends Exception {
  }
}
