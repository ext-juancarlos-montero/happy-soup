@isTest
public with sharing class PlusCouponActivationQuickAction_Test {
  @isTest
  private static void itShouldThrownAlreadyActiveException() {
    Plus_Coupon__c activePlusCoupon = new Plus_Coupon__c(
      Active__c = true,
      Country__c = 'ES'
    );
    insert activePlusCoupon;
    try {
      new PlusCouponActivationQuickAction().activate(activePlusCoupon.Id);
    } catch (Exception e) {
      System.assertEquals('Plus Coupon already active', e.getMessage());
    }
  }

  @isTest
  private static void itShouldThrownRequiredSpendingException() {
    Plus_Coupon__c plusCouponWithoutRequiredSpending = new Plus_Coupon__c(
      Active__c = false,
      Country__c = 'ES'
    );
    insert plusCouponWithoutRequiredSpending;
    try {
      new PlusCouponActivationQuickAction()
        .activate(plusCouponWithoutRequiredSpending.Id);
    } catch (Exception e) {
      System.assertEquals(
        'Required spending is required for activation',
        e.getMessage()
      );
    }
  }

  @isTest
  private static void itShouldThrownAtLeastOnePrizeRequiredException() {
    Plus_Coupon__c plusCouponWithoutAnyPrizeSet = new Plus_Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Required_Spending__c = 1
    );
    insert plusCouponWithoutAnyPrizeSet;
    try {
      new PlusCouponActivationQuickAction()
        .activate(plusCouponWithoutAnyPrizeSet.Id);
    } catch (Exception e) {
      System.assertEquals('At least one prize is required', e.getMessage());
    }
  }

  @isTest
  private static void itShouldThrownSomPrizesArentProperlyConfiguredException() {
    Plus_Coupon__c plusCouponWithoutProperlyConfiguredPrize = new Plus_Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Required_Spending__c = 1
    );
    insert plusCouponWithoutProperlyConfiguredPrize;
    Coupon__c prize = new Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Discount__c = 1.0,
      RecordTypeId = Schema.SObjectType.Coupon__c.getRecordTypeInfosByDeveloperName()
        .get('Prize_Coupon')
        .getRecordTypeId()
    );
    insert prize;
    String prizeName = [SELECT Name FROM Coupon__c WHERE Id = :prize.Id].Name;
    insert new Plus_Coupon_Prize__c(
      Coupon__c = prize.Id,
      Plus_Coupon__c = plusCouponWithoutProperlyConfiguredPrize.Id
    );
    try {
      new PlusCouponActivationQuickAction()
        .activate(plusCouponWithoutProperlyConfiguredPrize.Id);
    } catch (Exception e) {
      System.assertEquals(
        'Some prizes aren\'t properly configured:' + prizeName,
        e.getMessage()
      );
    }
  }

  @isTest
  private static void itShouldActivateThePlusCoupon() {
    Plus_Coupon__c plusCouponToActivate = new Plus_Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Required_Spending__c = 1
    );
    insert plusCouponToActivate;
    Coupon__c prize = new Coupon__c(
      Active__c = true,
      Country__c = 'ES',
      Discount__c = 1.0,
      RecordTypeId = Schema.SObjectType.Coupon__c.getRecordTypeInfosByDeveloperName()
        .get('Prize_Coupon')
        .getRecordTypeId()
    );
    insert prize;
    insert new Plus_Coupon_Prize__c(
      Coupon__c = prize.Id,
      Plus_Coupon__c = plusCouponToActivate.Id
    );
    new PlusCouponActivationQuickAction().activate(plusCouponToActivate.Id);
    System.assert(
      [SELECT Active__c FROM Plus_Coupon__c WHERE Id = :plusCouponToActivate.Id]
      .Active__c
    );
  }

  @isTest
  private static void itShouldThrownAlreadyDeactiveException() {
    Plus_Coupon__c inactivePlusCoupon = new Plus_Coupon__c(
      Active__c = false,
      Country__c = 'ES',
      Required_Spending__c = 1
    );
    insert inactivePlusCoupon;
    try {
      new PlusCouponActivationQuickAction().deactivate(inactivePlusCoupon.Id);
    } catch (Exception e) {
      System.assertEquals('Plus Coupon already inactive', e.getMessage());
    }
  }

  @isTest
  private static void itShouldDeactivateThePlusCoupon() {
    Plus_Coupon__c plusCouponToDeactivate = new Plus_Coupon__c(
      Active__c = true,
      Country__c = 'ES'
    );
    insert plusCouponToDeactivate;
    new PlusCouponActivationQuickAction().deactivate(plusCouponToDeactivate.Id);
    System.assert(
      ![
        SELECT Active__c
        FROM Plus_Coupon__c
        WHERE Id = :plusCouponToDeactivate.Id
      ]
      .Active__c
    );
  }
}
